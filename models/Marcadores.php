<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $url
 * @property string $descorta
 * @property string $deslarga
 * @property int $privado
 */
class Marcadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marcadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['privado'], 'integer'],
            [['nombre', 'url', 'descorta', 'deslarga'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'url' => 'Url',
            'descorta' => 'Descorta',
            'deslarga' => 'Deslarga',
            'privado' => 'Privado',
        ];
    }
}
