DROP DATABASE IF EXISTS practica4;
CREATE DATABASE IF NOT EXISTS practica4
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

-- 
-- Disable foreign keys
-- 
-- /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';
USE practica4;

--
-- Definition for table autores
--
CREATE TABLE IF NOT EXISTS marcadores (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar (255) DEFAULT NULL,
  url varchar (255) DEFAULT NULL,
  descorta varchar(255) DEFAULT NULL,
  deslarga varchar (255)DEFAULT NULL,
  privado boolean,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET latin1
COLLATE latin1_swedish_ci;



/*
-- Dumping data for tables entradas y fotos
--
INSERT INTO entradas VALUES
(1, 'Ejemplo de clase'),
(2, 'El Yii es facilisimo'),
(3, 'Mi mama me mima');

INSERT INTO fotos VALUES
(1, 'Izquierda','','alternativo1'),
(2, 'Centro','','alternativo2'),
(3, 'Derecha','','alternativo3');
  */